module "app-team-2" {
  source               = "./modules/multi-tenancy"
  application_projects = var.app-team-2
}

variable "app-team-2" {
  type = map(object({
    name        = string
    description = string
    size        = string
    groups = map(object({
      name  = string
      roles = list(string)
    }))
  }))
}