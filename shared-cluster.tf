resource "rafay_cluster_sharing" "test-cluster-4" {
  for_each = toset([
      "app-team-1",
      "app-team-2"
    ])
  clustername = "test-cluster-4"
  project     = "nimbus-eks-clusters"
  sharing {
    all = false
    projects {
      name = each.key
    }
  }
}