module "app-team-1" {
  source               = "./modules/multi-tenancy"
  application_projects = var.app-team-1
}

variable "app-team-1" {
  type = map(object({
    name        = string
    description = string
    size        = string
    groups = map(object({
      name  = string
      roles = list(string)
    }))
  }))
}
