terraform {
  backend "http" {
    address        = "https://gitlab.nimbus.amgen.com/api/v4/projects/28861/terraform/state/rafay"
    lock_address   = "https://gitlab.nimbus.amgen.com/api/v4/projects/28861/terraform/state/rafay/lock"
    unlock_address = "https://gitlab.nimbus.amgen.com/api/v4/projects/28861/terraform/state/rafay/lock"
  }
}
