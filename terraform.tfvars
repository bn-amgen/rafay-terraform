team-template = {
  team-template = {
    name        = "team-template"
    description = "Team Template"
    size        = "small"
    groups = {
      team-template = {
        name  = "team-template"
        roles = ["WORKSPACE_READ_ONLY"]
      }
    }
  }
}

app-team-1 = {
  app-team-1 = {
    name        = "app-team-1"
    description = "Application Team 1"
    size        = "small"
    groups = {
      app-team-1 = {
        name  = "app-team-1"
        roles = ["WORKSPACE_READ_ONLY"]
      }
    }
  }
}

app-team-2 = {
  app-team-2 = {
    name        = "app-team-2"
    description = "Application Team 2"
    size        = "small"
    groups = {
      app-team-2 = {
        name  = "app-team-2"
        roles = ["WORKSPACE_READ_ONLY"]
      }
    }
  }
}