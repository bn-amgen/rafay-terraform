module "team-template" {
  source               = "./modules/multi-tenancy"
  application_projects = var.team-template
}

variable "team-template" {
  type = map(object({
    name        = string
    description = string
    size        = string
    groups = map(object({
      name  = string
      roles = list(string)
    }))
  }))
}